package pageObjects;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GoogleSearchPageObject extends ChromeMainPageObject {
    protected WebDriver driver;

    public GoogleSearchPageObject(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    //Locators
    public static final String
            GOOGLE_SEARCH_FIELD = "[class='gLFyf']",
            GOOGLE_SEARCH_BUTTON = "[class='gNO89b']",
            GOOGLE_SEARCH_TITLES = "[class='LC20lb MBeuO DKV0Md']",
            GOOGLE_MAIN_PAGE_LOGO = "[class='lnXdpd']",
            GOOGLE_SEARCH_TABS = "//*[@class='bmaJhd iJddsb']";


    //Methods
    @Step("Click on {0} Google search result")
    public void clickOnSearchResultNotFassets(int num) {
        List<WebElement> titlesWebEl = driver.findElements(By.cssSelector(GOOGLE_SEARCH_TITLES));
        for (int i = 0; i < 20; i++) {
            try {
                titlesWebEl.get(num).click();
                break;
            } catch (ElementNotInteractableException e) {
                num++;
            }
        }
    }

    @Step("Search in Google method")
    public void searchInGoogle(String text, long timeoutInSec) {
        openCustomPage("https://www.google.com/");
        waitForElementAndTypeText(text, By.cssSelector(GOOGLE_SEARCH_FIELD), "Google search field not found", timeoutInSec);
        waitForElementPresentAndClick(By.cssSelector(GOOGLE_SEARCH_BUTTON), "Google search bttn not found", timeoutInSec);
    }

    @Step("Main Google page checks")
    public void checkMainGooglePage() {
        waitForElementNotPresent(By.cssSelector(GOOGLE_SEARCH_TITLES), "Search result titles appears!", 10);
        waitForElementPresent(By.cssSelector(GOOGLE_SEARCH_FIELD), "Google search field not found",10);
        waitForElementPresent(By.cssSelector(GOOGLE_MAIN_PAGE_LOGO), "Google logo not found",10);
    }

    @Step("Check for Google search results titles by word '{0}' method")
    public void checkTitlesAfterSearchInGoogle(String text) {
        List<WebElement> titlesWebEl = driver.findElements(By.cssSelector(GOOGLE_SEARCH_TITLES));
        List<String> titles = new ArrayList<>();
        titlesWebEl.stream().forEach(title -> titles.add(title.getAttribute("innerText")));
        assertTrue(titles.stream().anyMatch(title -> title.contains(text)), "There is no relevant search results");
    }

    @Step("Go to search tab {0}")
    public void goToSearchTab(int tabNum) {
        List<WebElement> tabs = driver.findElements(By.xpath(GOOGLE_SEARCH_TABS));
        try {
            tabs.get(tabNum).click();
        }
        catch (ElementNotInteractableException e) {
            assertNull(e,"Tab with number "+tabNum+" is not interactable");
        }
    }
}
