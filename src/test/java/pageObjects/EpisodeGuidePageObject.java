package pageObjects;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EpisodeGuidePageObject extends ChromeMainPageObject {
    protected WebDriver driver;

    public EpisodeGuidePageObject(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static final String
            HAMBURGER_MENU_ICON = ".global-nav__menu-icon",
            HAMBURGER_MENU_TABS = ".global-nav__menu__wrapper .global-nav__item.global-nav__item--link",
            STREAM_EPISODE_BTTN = ".hero__ctas .button--primary.cta--item",
            WATCH_PREVIEW_BTTN = ".hero__ctas .button--secondary.cta--item.button--video",
            MODAL_WINDOW = ".modal__content",
            MODAL_WINDOW_CLOSE_BTTN = ".modal__close",
            COOKIES_ACCEPT_BTTN = "#onetrust-accept-btn-handler",
            NEXT_ON_EPISODE_1_LABLE = ".video-metadata__details__title";


    //Methods
    @Step("Accept cookies if needed")
    public void coockiesAccept() {
        try {
            driver.findElement(By.cssSelector(COOKIES_ACCEPT_BTTN)).click();
        } catch (NoSuchElementException e) {}
    }

    @Step("Check element is on left by cssSelector")
    public void elemOnLeft(String locator) {
        int widthHalf = (driver.manage().window().getSize().getWidth()) / 2;
        int strEpisodePosition = driver.findElement(By.cssSelector(locator)).getLocation().x;
        //checking if bttns are left aligned
        assertTrue(strEpisodePosition < widthHalf, "Element is right based but should be left!");

    }

    @Step("Stream bttn functionality check")
    public void streamBttnFuncCheck() throws InterruptedException {
        //checking if bttns are left aligned
        elemOnLeft(STREAM_EPISODE_BTTN);
        elemOnLeft(WATCH_PREVIEW_BTTN);

        //waiting for modal window after clicking 'Stream episode' button
        waitForElementPresentAndClick(By.cssSelector(STREAM_EPISODE_BTTN), "'Stream episode' button not appears", 10);
        Thread.sleep(2_000);
        waitForElementPresent(By.cssSelector(MODAL_WINDOW), "Modal window not appers after clicking 'Stream episode' button", 10);

        //closing modal window check
        waitForElementPresentAndClick(By.cssSelector(MODAL_WINDOW_CLOSE_BTTN), "Modal window close bttn not found!", 10);
        Thread.sleep(2_000);
        waitForElementNotPresent(By.cssSelector(MODAL_WINDOW), "Modal window not appers after clicking 'Stream episode' button", 10);
    }

    @Step("Episode bttn functionality check")
    public void episodeBttnFuncCheck() {
        //waiting for video page loads for "Next on Episode 1" video
        waitForElementPresentAndClick(By.cssSelector(WATCH_PREVIEW_BTTN), "'Watch preview' button not appears", 10);
        waitForElementPresent(By.cssSelector(NEXT_ON_EPISODE_1_LABLE), "NEXT_ON_EPISODE_1 text not appers after clicking 'Watch preview' button", 10);
    }

    @Step("Hamburger menu checks")
    public void checkHamburgerTabs(String tabs) {
        // Click on burger menu bttn
        waitForElementPresentAndClick(By.cssSelector(HAMBURGER_MENU_ICON), "Hamburger btn not found", 15);

        List<String> expTabsList = new ArrayList<>();
        expTabsList.addAll(Arrays.asList(tabs.split(",")));

        List<WebElement> actualTabsWebElem = driver.findElements(By.cssSelector(HAMBURGER_MENU_TABS));
        List<String> actualTabsList = new ArrayList<>();

        for (WebElement word : actualTabsWebElem) {
            actualTabsList.add(word.getAttribute("outerText"));
        }

        assertAll(
                // Compare actual and expected lists of tabs
                () -> assertTrue(expTabsList.containsAll(actualTabsList) && expTabsList.size() == actualTabsList.size(), "Hamburger menu contains not all required tabs!"),
                // Check that first tab is not a link
                () -> assertFalse(actualTabsWebElem.get(0).getAttribute("innerHTML").contains("href="), "First tab must not be a link but it was!"),
                // Checking that remaining tabs are links
                () -> {
                    for (int i = 1; i < actualTabsWebElem.size(); i++) {
                        assertTrue(actualTabsWebElem.get(i).getAttribute("innerHTML").contains("href="), "Only first tab must be non link but " + i + 1 + " tab is also not a link!");
                    }
                },
                // Menu closes after clicking "X"
                () -> {
                    waitForElementPresentAndClick(By.cssSelector(HAMBURGER_MENU_ICON), "Close menu btn not found", 15);
                    waitForElementNotPresent(By.cssSelector(HAMBURGER_MENU_TABS), "Tabs are not closed!", 10);
                }
        );
    }


}
