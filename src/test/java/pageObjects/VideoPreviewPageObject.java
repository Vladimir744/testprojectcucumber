package pageObjects;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class VideoPreviewPageObject extends ChromeMainPageObject {
    protected WebDriver driver;

    public VideoPreviewPageObject(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static final String
            VIDEO_PLAYER_PLAYING = "[title='Pause']",
            VIDEO_PLAYER_PAUSED = "[title='Play']",
            VIDEO_PLAYER_PLAY_BTTN = "div.vjs-control-bar .vjs-play-control",
            VIDEO_PLAYER_VOLUME_BTTN = "div.vjs-control-bar .vjs-volume-panel",
            VIDEO_PLAYER_PICTURE_IN_PICTURE_BTTN = ".vjs-picture-in-picture-control.vjs-button",
            VIDEO_PLAYER_FULL_SCREEN_BTTN = ".vjs-fullscreen-control.vjs-button",
            VIDEO_PLAYER_VOLUME_SLIDER = "div.vjs-control-bar .vjs-volume-control",
            VIDEO_PLAYER_TIMELINE_SLIDER = ".vjs-progress-holder.vjs-slider",
            VIDEO_PLAYER_CURRENT_TIMER = ".vjs-current-time-display",
            VIDEO_PLAYER_DEVIDER_TIMER = ".vjs-time-divider",
            VIDEO_PLAYER_DURATION_TIMER = ".vjs-duration-display",
            VIDEO_PLAYER_FULL_SCREEN_ON = "[title='Exit Fullscreen']",
            VIDEO_PLAYER_FULL_SCREEN_OFF = "[title='Fullscreen']";

    //Methods
    @Step("Video player necessary elements check")
    public void videoPlayerNecessaryElemsCheck() {
        // Looking for volume on/off bttn
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_VOLUME_BTTN), "Volume on/off bttn not found!", 10);

        // Looking for volume slider
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_VOLUME_SLIDER), "Volume slider not found!", 10);

        // Looking for timeline slider
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_TIMELINE_SLIDER), "Timeline slider not found!", 10);

        // Looking for PIP bttn
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_PICTURE_IN_PICTURE_BTTN), "Picture in picture bttn not found!", 10);

        // Looking for full display bttn
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_FULL_SCREEN_BTTN), "Full display bttn not found!", 10);

        // Looking for timer and its format
        String timer = driver.findElement(By.cssSelector(VIDEO_PLAYER_CURRENT_TIMER)).getAttribute("outerText") +
                driver.findElement(By.cssSelector(VIDEO_PLAYER_DEVIDER_TIMER)).getAttribute("outerText") +
                driver.findElement(By.cssSelector(VIDEO_PLAYER_DURATION_TIMER)).getAttribute("outerText");
        Matcher matcher = Pattern.compile("^\\d:\\d\\d/\\d:\\d\\d$").matcher(timer);
        assertTrue(matcher.matches(), "Timer format not correct!");
    }

    @Step("Video player play/stop button check")
    public void playStopBttnCheck() {
        try {
            waitForElementPresentAndClick(By.cssSelector(VIDEO_PLAYER_PLAY_BTTN), "Play bttn not found!", 10);
            waitForElementPresent(By.cssSelector(VIDEO_PLAYER_PAUSED), "Video should be paused!", 10);
        } catch (NoSuchElementException e) {
            waitForElementPresentAndClick(By.cssSelector(VIDEO_PLAYER_PLAY_BTTN), "Play bttn not found!", 10);
        }
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_PAUSED), "Video should be paused!", 10);
        waitForElementPresentAndClick(By.cssSelector(VIDEO_PLAYER_PLAY_BTTN), "Play bttn not found!", 10);
    }


    @Step("Video player functionality checks after opening episode page")
    public void videoPlayerFuncCheck() {
        // Checking autoplay
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_PLAYING), "Video is not autoplay but should!", 10);

        // Looking for play/pause bttn and check it works
        playStopBttnCheck();

        // Checking for necessary elements of video player
        videoPlayerNecessaryElemsCheck();
    }

    @Step("Video player full screen functionality checks")
    public void videoPlayerFullScreenFuncCheck() throws InterruptedException {
        waitForElementPresentAndClick(By.cssSelector(VIDEO_PLAYER_FULL_SCREEN_BTTN), "Full display bttn not found!", 10);
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_FULL_SCREEN_ON), "Full display mode was not on!", 10);
        Thread.sleep(2_000);
        videoPlayerFuncCheck();
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE).build().perform();
        waitForElementPresent(By.cssSelector(VIDEO_PLAYER_FULL_SCREEN_OFF), "Full display mode was not off after pressing ESC bttn!", 10);
    }
}
