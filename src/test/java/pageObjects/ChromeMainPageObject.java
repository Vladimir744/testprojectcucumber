package pageObjects;

import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ChromeMainPageObject {
    // SANDBOX PAGE OBJECT!
    protected WebDriver driver;

    public ChromeMainPageObject(WebDriver driver) {
        this.driver = driver;
    }

    public static final String

            ELEMENT_CONTAINS_TEXT = "//*[contains(text(), '{SUBSTRING}')]",
            URL_STARTS_WITH = "a[href^='{SUBSTRING}']",
            ELEM_BY_CUSTOM_CLASS = ".{SUBSTRING}",
            SEARCH_WITH_PARAM = "[{PARAM}='{VALUE}']";


    @Step("Open URL {0}")
    public void openCustomPage(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }

    @Step("Open URL {0} in Incognito")
    public void openCustomPageInIncognito(String url) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver.get(url);
        driver.manage().window().maximize();
    }

    @Step("Wait for element {0} present")
    public WebElement waitForElementPresent(By by, String err_mes, long timeoutInSec) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSec);
        wait.withMessage(err_mes + "\n");
        return wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    @Step("Wait for element {0} not present")
    public boolean waitForElementNotPresent(By by, String err_mes, long timeoutInSec) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSec);
        wait.withMessage(err_mes + "\n");
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    @Step("Wait for element {1} and type text {0}")
    public void waitForElementAndTypeText(String text, By by, String err_mes, long timeoutInSec) {
        WebElement field = waitForElementPresent(by, err_mes, timeoutInSec);
        field.click();
        field.clear();
        field.sendKeys(text);
    }

    @Step("Wait for element {0} and click")
    public void waitForElementPresentAndClick(By by, String err_mes, long timeoutInSec) {
        waitForElementPresent(by, err_mes, timeoutInSec).click();
    }

    @Step("Search element with param {0} which equals {1}")
    public void searchWithParam(String param, String value) {
        String loc = SEARCH_WITH_PARAM.replace("{PARAM}", param).replace("{VALUE}", value);
        WebElement element = waitForElementPresent(By.cssSelector(loc), "No element with '" + param + "' and value '" + value + "'", 15);
    }

    @Step("Click for element with attribute {0} equals {1}")
    public void clickElementByCssAttribute(String attr, String class_name) {
        WebElement element = waitForElementPresent(By.cssSelector("["+attr+"='"+class_name+"']"), "No element with class " + class_name, 15);
        element.click();
    }

    @Step("Return all elements with URL starts with {0}")
    public List<WebElement> findAllElementsWithUrlStartsWith(String text) {
        String searchedUrl = URL_STARTS_WITH.replace("{SUBSTRING}", text);
        return driver.findElements(By.cssSelector(searchedUrl));
    }

    @Step("Scroll to some element {0}")
    public void scrollToSomeElementByCss(String by) {
        WebElement pages = driver.findElement(By.cssSelector(by));
        int maxCount = 40;
        Actions actions = new Actions(driver);

        for (int i = 0; i < maxCount; i++) {
            if (pages.isDisplayed()) {
                actions.sendKeys(Keys.PAGE_DOWN);
                actions.perform();
            } else {
                System.out.println("Element found after " + i + " scrols down");
                break;
            }
        }
    }

    @Step("Scroll {0} times")
    public void scrollDownCustomTimes(int times) {
        Actions actions = new Actions(driver);
        for (int i = 0; i < times; i++) {
            actions.sendKeys(Keys.PAGE_DOWN);
            actions.perform();
        }
    }

    @Step("Return elements contains text {0}")
    public List<WebElement> returnListElementContainsText(String text) {
        String searchedText = ELEMENT_CONTAINS_TEXT.replace("{SUBSTRING}", text);
        return driver.findElements(By.xpath(searchedText));
    }

    @Step("Wait for element contains text {0} and click")
    public void waitElementContainsTextAndClick(String text) {
        List<WebElement> list = returnListElementContainsText(text);
        int i = 0;
        for (WebElement item : list) {
            try {
                list.get(i).click();
                break;
            }
            catch (ElementNotInteractableException e) {
                i++;
            }
        }
        assertTrue(i<list.size(),"There is no clickable element with text "+text);
        System.out.println("Find "+list.size()+" elements contains text '"+text+"'. Element number "+i+" clicked.");
    }

    @Step("Wait for element contains text {0}")
    public void waitElementContainsTextAssertion(String text) {
        List<WebElement> list = returnListElementContainsText(text);
        assertTrue(list.size()>0, "Element with text '"+text+"' not found");
    }

    @Step("Hover element and get title attr")
    public void hoverElementByClassAndCheckTitle(String text, int timeInMillis, String title) throws InterruptedException {
        String loc = ELEM_BY_CUSTOM_CLASS.replace("{SUBSTRING}", text);
        WebElement ele = driver.findElement(By.cssSelector(loc));

        Actions action = new Actions(driver);
        action.moveToElement(ele).perform();

        Thread.sleep(timeInMillis);

        assertEquals(title, ele.getAttribute("title"), "Tooltip is not correct");
    }
}
