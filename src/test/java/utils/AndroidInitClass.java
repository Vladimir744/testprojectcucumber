package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Step;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AndroidInitClass {
    private static AppiumDriver driver;

    @Step("Initializing and return AppiumDriver")
    public static AppiumDriver setUpAppiumDriver() throws MalformedURLException {
        //init AppiumDriver
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "AndroidTestEmulator");
        capabilities.setCapability("platformVersion", "9.0");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", "com.ambertech.amber");
        capabilities.setCapability("appActivity", ".MainActivity");
        capabilities.setCapability("app", "/Users/annaklimakina/IdeaProjects/TestProject/src/apks/Picnic.apk");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        return driver;
    }

    @AfterEach
    @Step("Tear down")
    public void tearDown() {
        driver.quit();
    }
}
