package utils;

import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HttpRequests {

    private static final String token = "6469160356:AAFhXbOCze-49-leu-jzSrcAoE80YtTcujM";
    private static final String chat_id = "650377988";

    private static final String domain = "https://api.telegram.org/bot"+token+"/sendMessage?chat_id="+chat_id+"&text=";

    private static final String gitlabToken = "glpat-VzuFdjBpyi_P-L7vy1rK";
    private static final String gitlabApiUrl = "https://gitlab.com/api/v4/projects/42731537/pipeline_schedules/";





    public static void sendPostRequestToTelegram(String mes, int expectedStatusCode) {
        Response resp = given()
                .post(domain+mes);

        assertEquals(expectedStatusCode, resp.getStatusCode(), "Unexpected status code "
                + resp.statusCode() + ". Response was \n" + resp.body().asString());
    }

    public static Response sendGetRequestToGitForScheduleInfo(int expectedStatusCode) {
        Response resp = given()
                .header("PRIVATE-TOKEN", gitlabToken)
                .get(gitlabApiUrl);

        assertEquals(expectedStatusCode, resp.getStatusCode(), "Unexpected status code "
                + resp.statusCode() + ". Response was \n" + resp.body().asString());
        return resp;
    }

    public static Response sendDeleteRequestToGitForScheduleDelete(int expectedStatusCode, String pipelineScheduleId) {
        Response resp = given()
                .header("PRIVATE-TOKEN", gitlabToken)
                .delete(gitlabApiUrl+pipelineScheduleId);

        assertEquals(expectedStatusCode, resp.getStatusCode(), "Unexpected status code "
                + resp.statusCode() + ". Response was \n" + resp.body().asString());
        return resp;
    }

    public static String sendGetRequestToGitForScheduleId(int expectedStatusCode, String name) {
        Response resp = given()
                .header("PRIVATE-TOKEN", gitlabToken)
                .get(gitlabApiUrl);

        List<String> descriptions = resp.body().path("description");
        List<String> ids = resp.body().path("id");

        String id = String.valueOf(ids.get(descriptions.indexOf(name)));

        assertEquals(expectedStatusCode, resp.getStatusCode(), "Unexpected status code "
                + resp.statusCode() + ". Response was \n" + resp.body().asString());
        return id;
    }
}
