package tests.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import utils.ChromeInitClass;
import utils.HttpRequests;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;

@DisplayName("Local sandBox")
public class WaitTest extends ChromeInitClass {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";

//    ChromeMainPageObject chromeMainPageObject = new ChromeMainPageObject(setUpChromeDriver());

    // Optimised waiting util
    @Test
    public void testTest() throws InterruptedException {
        Calendar calendar = Calendar.getInstance();
        int day = parseInt(new SimpleDateFormat("dd").format(calendar.getTime()));
        int month = parseInt(new SimpleDateFormat("MM").format(calendar.getTime()));


        int tillYear = 2023;
        int tillMonth = 9;
        int tillDay = 28;
        int tillHour = 17;
        int tillMinute = 50;


        int fromYear = 2023;
        int fromMonth = 8;
        int fromDay = 25;
        int fromHour = 11;
        int fromMinute = 05;


        float daysBetweenTwoDates = getDaysBetweenDates(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute);
        float daysPassed = daysBetweenTwoDates - getDaysBetweenDateAndCurrent(tillYear, tillMonth, tillDay, tillHour, tillMinute);

        float persentDone = (daysPassed * 100) / daysBetweenTwoDates;
        float persentRem = 100 - persentDone;


        System.out.println("----------------------------------------------------------------------------------------------------");
        System.out.println();


        if (persentDone >= 100) {
            persentDone = 100;
            System.out.println("YOU HAVE REACHED THE EVENT! CONGRATULATIONS!");
            for (int i = 0; i <= 50; i++) {
                System.out.print(" ");
            }
            System.out.println(persentDone + " % / 100 %");
            System.out.print(ANSI_GREEN + "======================================================================================================" + ANSI_RESET);


            System.out.println();
            System.out.println("- Remain persent: 0 %");
            System.out.println("- Passed time: " + getDaysBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " days " + getHoursBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " hours " + getMinutesBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " minutes");
            System.out.println();

        } else {
            Thread.sleep(61000);

            float persentAtTheEndOfDay = ((getPersentDone(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) - persentDone) * (float) (getHoursToDate(tillYear, month, day + 1, 00, 00) * 60)) + persentDone;

            for (int i = 0; i <= 50; i++) {
                System.out.print(" ");
            }
            System.out.println(persentDone + " % / 100 %");

            for (int i = 0; i <= persentDone; i++) {
                System.out.print(ANSI_GREEN + "=" + ANSI_RESET);
            }
            for (int i = 0; i <= 100 - persentDone; i++) {
                System.out.print(".");
            }
            System.out.println();
            System.out.println("- Remain persent: " + persentRem + " %");
            System.out.println("- Estimated persent at the end of a day: " + persentAtTheEndOfDay + " %");
            System.out.println();
            System.out.println("- Passed time: " + getDaysToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " days " + getHoursToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " hours " + getMinutesToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " minutes");
            System.out.println("- Remain days: " + getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " days " + getHoursToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " hours " + getMinutesToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " minutes");
            System.out.println("- Remain weeks: " + (getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) / 7 + " weeks " + ((int) (getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) - (int) ((getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) / 7) * 7) + " days");
            System.out.println("- Total time between dates: " + getDaysBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " days " + getHoursBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " hours " + getMinutesBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " minutes");
            System.out.println();
            

//      **************************************** SENDING INFO TO TELEGRAM BOT ENG ****************************************

//            if (persentDone >= 99) {
//                String message = "YOU HAVE REACHED THE EVENT! CONGRATULATIONS!\n\n" +
//                        "- Total time passed: " + getDaysBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " days " + getHoursBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " hours " + getMinutesBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " minutes";
//                HttpRequests.sendPostRequestToTelegram(message, 200);
//                String scedId = HttpRequests.sendGetRequestToGitForScheduleId(200, "Timer");
//                HttpRequests.sendDeleteRequestToGitForScheduleDelete(200,scedId);
//            }
//            else {
//                String message = "- Complete: " + persentDone + " % / 100 % \n\n" +
//                        "- Remain persent: " + persentRem + " % \n\n" +
//                        "- Estimated persent at the end of a day: " + persentAtTheEndOfDay + " %\n\n" +
//                        "- Passed time: " + getDaysToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " days " + getHoursToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " hours " + getMinutesToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " minutes\n\n" +
//                        "- Remain days: " + getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " days " + getHoursToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " hours " + getMinutesToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " minutes\n\n" +
//                        "- Remain weeks: " + (getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) / 7 + " weeks " + ((int) (getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) - (int) ((getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) / 7) * 7) + " days\n\n" +
//                        "- Total time between dates: " + getDaysBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " days " + getHoursBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " hours " + getMinutesBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " minutes\n\n";
//
//                // % PERCENT CONDITIONS
//                if (persentDone >= 50 && persentDone <= 55) {
//                    message = message+"\nITS 50% ALREADY DONE!!! ITS A HALF!";
//                } else if (persentDone >= 25 && persentDone <= 30) {
//                    message = message+"\nITS 25% ALREADY DONE!!! ITS A QUATER!";
//                } else if (persentDone >= 75 && persentDone <= 80) {
//                    message = message+"\nITS 75% ALREADY DONE!!! ITS A 3/4!";
//
//                }
////        else {
////            System.out.println();
////        }
//
//                // DAYS CONDITIONS
//                if (day == 1) {
//                    message = message+"\nMY DEAR WILL COME THIS MONTH!";
//                } else if (day == 30) {
//                    message = message+"\n4 WEEKS REMAIN";
//                } else if (day == 7) {
//                    message = message+"\n3 WEEKS REMAIN";
//                } else if (day == 14) {
//                    message = message+"\n2 WEEKS REMAIN";
//                } else if (day == 23 && month == 10) {
//                    message = message+"\nTHIS WEEK MY LOVE WILL ARRIVE";
//                }
//                HttpRequests.sendPostRequestToTelegram(message, 200);
//            }


            //      **************************************** SENDING INFO TO TELEGRAM BOT RUS ****************************************

            if (persentDone >= 99) {
                String message = "СОБЫТИЕ ДОСТИГНУТО! УРААААА!!!\n\n" +
                        "- Прошло времени: " + getDaysBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " дней " + getHoursBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " часов " + getMinutesBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " минут";
                HttpRequests.sendPostRequestToTelegram(message, 200);
                String scedId = HttpRequests.sendGetRequestToGitForScheduleId(200, "Timer");
                HttpRequests.sendDeleteRequestToGitForScheduleDelete(200, scedId);
            } else {
                String message = "- Прошло времени в процентах: " + persentDone + " % / 100 % \n\n" +
                        "- Осталось процентов: " + persentRem + " % \n\n" +
                        "- Расчетный пройденный процент в конце дня: " + persentAtTheEndOfDay + " %\n\n" +
                        "- Времени прошло: " + getDaysToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " дней " + getHoursToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " часов " + getMinutesToDate(fromYear, fromMonth, fromDay, fromHour, fromMinute) + " минут\n\n" +
                        "- Осталось: " + getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " дней " + getHoursToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " часов " + getMinutesToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute) + " минут\n\n" +
                        "- Осталось в неделях: " + (getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) / 7 + " недель " + ((int) (getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) - (int) ((getDaysToDate(tillYear, tillMonth, tillDay, tillHour, tillMinute)) / 7) * 7) + " дней\n\n" +
                        "- Общий промежуток между датами: " + getDaysBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " дней " + getHoursBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " часов " + getMinutesBetween(fromYear, fromMonth, fromDay, fromHour, fromMinute, tillYear, tillMonth, tillDay, tillHour, tillMinute) + " минут\n\n";
//             % PERCENT CONDITIONS
                if (persentDone >= 50 && persentDone <= 55) {
                    message = message + "\n50% ПРОЙДЕНО!!! ПОЛОВИНА УРААА!";
                } else if (persentDone >= 25 && persentDone <= 30) {
                    message = message + "\n25% ПРОЙДЕНО!!! ЧЕТВЕРТЬ, ЕЩЕ СТОЛЬКО ЖЕ 3 РАЗА!!";
                } else if (persentDone >= 75 && persentDone <= 80) {
                    message = message + "\n75% ПРОЙДЕНО!!! ЭТО 3/4! ЕЩЕ ЧУТЬ-ЧУТЬ!";

                }
//        else {
//            System.out.println();
//        }

                // DAYS CONDITIONS
                if (day == 1) {
                    message = message + "\nУЖЕ В ЭТОМ МЕСЯЦЕ ОБНИМУ ТЕБЯ!";
                } else if (day == 30) {
                    message = message + "\nРОВНО 4 НЕДЕЛИ ОСТАЛОСЬ ДО ПРИЕЗДА";
                } else if (day == 7) {
                    message = message + "\nРОВНО 3 НЕДЕЛИ ОСТАЛОСЬ ДО ПРИЕЗДА";
                } else if (day == 14) {
                    message = message + "\nРОВНО 2 НЕДЕЛИ ОСТАЛОСЬ ДО ПРИЕЗДА";
                } else if (day == 21 && month == 10) {
                    message = message + "\nРОВНО 1 НЕДЕЛЯ ОСТАЛАСЬ ДО ПРИЕЗДА";
                } else if (day == 23 && month == 10) {
                    message = message + "\nУЖЕ НА ЭТОЙ НЕДЕЛЕ ОБНИМУ ТЕБЯ!";
                } else if (day == 25 && month == 10) {
                    message = message + "\nСЕГОДНЯ В МОСКВУ!";
                }
                HttpRequests.sendPostRequestToTelegram(message, 200);
            }
//
        }


//------------------------------------------------------------------------------------
        // % PERCENT CONDITIONS
        if (persentDone >= 50 && persentDone <= 55) {
            System.out.println(ANSI_GREEN + "----- ITS 50% ALREADY DONE!!! ITS A HALF! -----" + ANSI_RESET);
            System.out.println();
        } else if (persentDone >= 25 && persentDone <= 30) {
            System.out.println(ANSI_GREEN + "----- ITS 25% ALREADY DONE!!! ITS A QUATER! -----" + ANSI_RESET);
            System.out.println();
        } else if (persentDone >= 75 && persentDone <= 80) {
            System.out.println(ANSI_GREEN + "----- ITS 75% ALREADY DONE!!! ITS A 3/4! -----" + ANSI_RESET);
            System.out.println();
        }
//        else {
//            System.out.println();
//        }

        // DAYS CONDITIONS
        if (day == 1) {
            System.out.println(ANSI_GREEN + "----- MY DEAR WILL COME THIS MONTH -----" + ANSI_RESET);
            System.out.println();
        } else if (day == 30) {
            System.out.println(ANSI_GREEN + "----- 4 WEEKS REMAIN -----" + ANSI_RESET);
            System.out.println();
        } else if (day == 7) {
            System.out.println(ANSI_GREEN + "----- 3 WEEKS REMAIN -----" + ANSI_RESET);
            System.out.println();
        } else if (day == 14) {
            System.out.println(ANSI_GREEN + "----- 2 WEEKS REMAIN -----" + ANSI_RESET);
            System.out.println();
        } else if (day == 21 && month == 10) {
            System.out.println(ANSI_GREEN + "----- 2 WEEKS REMAIN -----" + ANSI_RESET);
            System.out.println();
        } else if (day == 23 && month == 10) {
            System.out.println(ANSI_GREEN + "----- THIS WEEK MY LOVE WILL ARRIVE -----" + ANSI_RESET);
            System.out.println();
        }
        //------------------------------------------------------------------------------------

        System.out.println("----------------------------------------------------------------------------------------------------");
    }


    public float getDaysBetweenDateAndCurrent(int year, int month, int day, int hour, int minute) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();

        long end = calendar.getTimeInMillis();
        long start = calendar1.getTimeInMillis();
        long millisBetween = end - start;
        float days = (float) ((float) millisBetween / 86400000.0);

        return days;
    }

    public float getDaysBetweenDates(int year, int month, int day, int hour, int minute, int yearEnd, int monthEnd, int dayEnd, int hourEnd, int minuteEnd) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();
        calendar1.set(Calendar.YEAR, yearEnd);
        calendar1.set(Calendar.MONTH, monthEnd);
        calendar1.set(Calendar.DAY_OF_MONTH, dayEnd);
        calendar1.set(Calendar.HOUR_OF_DAY, hourEnd);
        calendar1.set(Calendar.MINUTE, minuteEnd);
        calendar1.set(Calendar.SECOND, 00);


        long end = calendar1.getTimeInMillis();
        long start = calendar.getTimeInMillis();
        long millisBetween = end - start;
        float days = (float) ((float) millisBetween / 86400000.0);

        return days;
    }


    public long getDaysBetween(int year, int month, int day, int hour, int minute, int tillYear, int tillMonth, int tillDay, int tillHour, int tillMinute) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();
        calendar1.set(Calendar.YEAR, tillYear);
        calendar1.set(Calendar.MONTH, tillMonth);
        calendar1.set(Calendar.DAY_OF_MONTH, tillDay);
        calendar1.set(Calendar.HOUR_OF_DAY, tillHour);
        calendar1.set(Calendar.MINUTE, tillMinute);
        calendar1.set(Calendar.SECOND, 00);

        long end = calendar.getTimeInMillis();
        long start = calendar1.getTimeInMillis();
        long millisBetween = end - start;
        long days = (long) (millisBetween / 86400000.0);

        if (days < 0) {
            days = days * (-1);
        }
        return days;
    }

    public int getHoursBetween(int year, int month, int day, int hour, int minute, int tillYear, int tillMonth, int tillDay, int tillHour, int tillMinute) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();
        calendar1.set(Calendar.YEAR, tillYear);
        calendar1.set(Calendar.MONTH, tillMonth);
        calendar1.set(Calendar.DAY_OF_MONTH, tillDay);
        calendar1.set(Calendar.HOUR_OF_DAY, tillHour);
        calendar1.set(Calendar.MINUTE, tillMinute);
        calendar1.set(Calendar.SECOND, 00);

        long end = calendar.getTimeInMillis();
        long start = calendar1.getTimeInMillis();
        long millisBetween = end - start;
        float days = (float) ((float) millisBetween / 86400000.0);
        String[] hoursDev = Float.toString(days).split("\\.", 2);
        String hoursFloat = "0." + hoursDev[1];
        int hoursRem = (int) ((Float.parseFloat(hoursFloat)) * 24) / 1;
        if (hoursRem < 0) {
            hoursRem = hoursRem * (-1);
        }
        return hoursRem;
    }

    public int getMinutesBetween(int year, int month, int day, int hour, int minute, int tillYear, int tillMonth, int tillDay, int tillHour, int tillMinute) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();
        calendar1.set(Calendar.YEAR, tillYear);
        calendar1.set(Calendar.MONTH, tillMonth);
        calendar1.set(Calendar.DAY_OF_MONTH, tillDay);
        calendar1.set(Calendar.HOUR_OF_DAY, tillHour);
        calendar1.set(Calendar.MINUTE, tillMinute);
        calendar1.set(Calendar.SECOND, 00);

        long end = calendar.getTimeInMillis();
        long start = calendar1.getTimeInMillis();
        long millisBetween = end - start;
        float days = (float) ((float) millisBetween / 86400000.0);
        String[] hoursDev = Float.toString(days).split("\\.", 2);
        String hoursFloat = "0." + hoursDev[1];
        float hoursRem = ((Float.parseFloat(hoursFloat)) * 24) / 1;
        String[] minsDev = Float.toString(hoursRem).split("\\.", 2);
        String minsFloat = "0." + minsDev[1];
        int minsRem = (int) ((Float.parseFloat(minsFloat)) * 60) / 1;
        if (minsRem < 0) {
            minsRem = minsRem * (-1);
        }
        return minsRem;
    }

    public long getDaysToDate(int year, int month, int day, int hour, int minute) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();
        long end = calendar.getTimeInMillis();
        long start = calendar1.getTimeInMillis();
        long millisBetween = end - start;
        long days = (long) (millisBetween / 86400000.0);

        if (days < 0) {
            days = days * (-1);
        }
        return days;
    }

    public int getHoursToDate(int year, int month, int day, int hour, int minute) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();
        long end = calendar.getTimeInMillis();
        long start = calendar1.getTimeInMillis();
        long millisBetween = end - start;
        float days = (float) ((float) millisBetween / 86400000.0);
        String[] hoursDev = Float.toString(days).split("\\.", 2);
        String hoursFloat = "0." + hoursDev[1];
        int hoursRem = (int) ((Float.parseFloat(hoursFloat)) * 24) / 1;
        if (hoursRem < 0) {
            hoursRem = hoursRem * (-1);
        }
        return hoursRem;
    }

    public int getMinutesToDate(int year, int month, int day, int hour, int minute) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar1 = new GregorianCalendar();
        long end = calendar.getTimeInMillis();
        long start = calendar1.getTimeInMillis();
        long millisBetween = end - start;
        float days = (float) ((float) millisBetween / 86400000.0);
        String[] hoursDev = Float.toString(days).split("\\.", 2);
        String hoursFloat = "0." + hoursDev[1];
        float hoursRem = ((Float.parseFloat(hoursFloat)) * 24) / 1;
        String[] minsDev = Float.toString(hoursRem).split("\\.", 2);
        String minsFloat = "0." + minsDev[1];
        int minsRem = (int) ((Float.parseFloat(minsFloat)) * 60) / 1;
        if (minsRem < 0) {
            minsRem = minsRem * (-1);
        }
        return minsRem;
    }

    public static float getPersentDone() {
        Calendar calendar = Calendar.getInstance();
        int day = parseInt(new SimpleDateFormat("dd").format(calendar.getTime()));
        int hour = parseInt(new SimpleDateFormat("HH").format(calendar.getTime()));
        int minute = parseInt(new SimpleDateFormat("mm").format(calendar.getTime()));

        String dash2 = Float.toString(((hour + ((float) minute / 60)) * 100) / 24).replaceAll("\\.", "");
        int dash = Integer.parseInt(dash2);

        float daysRem = (float) 24.20;
        float current = Float.parseFloat(day + "." + dash);
        float persentDone = (current * 100) / daysRem;
        return persentDone;
    }

    public float getPersentDone(int year, int month, int day, int hour, int minute, int tillYear, int tillMonth, int tillDay, int tillHour, int tillMinute) {
        float daysBetweenTwoDates = getDaysBetweenDates(year, month, day, hour, minute, tillYear, tillMonth, tillDay, tillHour, tillMinute);
        float daysPassed = daysBetweenTwoDates - getDaysBetweenDateAndCurrent(tillYear, tillMonth, tillDay, tillHour, tillMinute);
        float persentDone = (daysPassed * 100) / daysBetweenTwoDates;
        return persentDone;
    }
}
