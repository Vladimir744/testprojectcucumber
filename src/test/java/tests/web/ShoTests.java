package tests.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import pageObjects.ChromeMainPageObject;
import pageObjects.EpisodeGuidePageObject;
import pageObjects.VideoPreviewPageObject;
import utils.ChromeInitClass;

import java.awt.*;

public class ShoTests extends ChromeInitClass {
    private final String mainUrl = "https://www.sho.com";
    private final WebDriver driver = setUpChromeDriver();
    ChromeMainPageObject chromeMainPageObject = new ChromeMainPageObject(driver);
    VideoPreviewPageObject videoPreviewPageObject = new VideoPreviewPageObject(driver);
    EpisodeGuidePageObject episodeGuidePageObject = new EpisodeGuidePageObject(driver);

    @BeforeEach
    public void getPageAndAcceptCookies() {
        chromeMainPageObject.openCustomPageInIncognito(mainUrl + "/homeland/season/5/episode/1/separation-anxiety");
        episodeGuidePageObject.coockiesAccept();
    }

    @Test
    @DisplayName("Checking for hamburger menu tabs.")
    public void checkHamburgerTabsTest() {
        episodeGuidePageObject.checkHamburgerTabs("Series,Movies,Sports,Documentaries");
    }

    @Test
    @DisplayName("Verify Episode Guide page functionality.")
    public void episodeGuidePageTest() throws InterruptedException {
        episodeGuidePageObject.streamBttnFuncCheck();
    }

    @Test
    @DisplayName("Click the Watch a Preview CTA.")
    public void clickingWatchPreviewTest() {
        episodeGuidePageObject.episodeBttnFuncCheck();
    }

    @Test
    @DisplayName("Verify video player functionality during playback.")
    public void videoPlayerFuncTest() throws InterruptedException, AWTException {
        videoPreviewPageObject.videoPlayerFuncCheck();
        videoPreviewPageObject.videoPlayerFullScreenFuncCheck();
    }
}
