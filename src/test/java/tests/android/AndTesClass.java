package tests.android;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;

import static utils.AndroidInitClass.setUpAppiumDriver;

public class AndTesClass {
    private final WebDriver driver = setUpAppiumDriver();

    public AndTesClass() throws MalformedURLException {
    }
    
    @Test
    public void firstTest() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("//android.view.View[@content-desc=\"get started\"]")).click();
        Thread.sleep(3000);
    }
}
